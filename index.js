const express=require('express');
const {data} = require('./data');
const Joi = require('joi');

const app = express();
app.use(express.json());

app.get("/",(req, res)=>{
    res.send("root of web app");
});

app.get("/books",(req, res)=>{
    res.send(data);
});


app.get("/books/:id",(req, res)=>{
    const book = data.find(a=>a.id === parseInt(req.params.id));

    if(! book){
        res.status(404).send("no book found for the given id");
    }
    
    res.send(book);
});

const port = process.env.PORT || 3000;
app.listen(port, ()=>{
    console.log(`node is listening on port ${port}`);
});
